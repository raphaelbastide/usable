---
project_name: G.Projector
version:  1.8
current_version_release_date: 2015-09-18
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-12-19
project_first-release: 2007-03-13
# Is the project developpement still active?
still_active: true
license:
taxonomy:
  creators: [Robert B.Schmunk]
  plateforms: [linux, windows, mac]
  categories: [generator]
  tags: []
project_url: http://www.giss.nasa.gov/tools/gprojector/
extra_links:
  tutorials: http://www.giss.nasa.gov/tools/gprojector/help/
technology: Java
dependency:
installation_process: >
  - Set Up

# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

G.Projector is a Java application which allows you to explore a large collection ofglobal map projections and optionally project an an input  GIF, JPG or PNG equirectangular map image.

## Detailed Description

G.Projector is a cross-platform application which can transform an equirectangular map image into one of over 100 global and regional map projections. Longitude-latitude gridlines and continental outlines may be drawn on the map, and the resulting image may be saved to disk in GIF, JPEG, PDF, PNG, PS or TIFF form.
