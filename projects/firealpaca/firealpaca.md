---
project_name: FireAlcapa
version: 1.5.0
current_version_release_date:2015.10.12
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015.11.08
project_first-release: 2011.10.10
# Is the project developpement still active?
still_active: true
license: Free
taxonomy:
  creators: [Unknow for now]
  plateforms: [windows, mac]
  categories: [Images]
  tags: [ ]
project_url: http://firealpaca.com/fr
extra_links:
  forum: http://everythingfirealpaca.tumblr.com/
technology: Python
dependency: MediBang Paint
installation_process: >
  - Step Up
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

FireAlpaca is an image editor.

## Detailed Description

You can create or edit image easly with this software. One of the thing that really cool with Firealpace, is that you can use your own brush, without script it : you can use a .png for exemple and you have the possibility to use it as a pattern. Some grid are also avaible even if they are quiet simple, you can make some intressting thing with it. It's a simple software to use.
