---
project_name: TexTuring
version: 2.3
# Is the project developpement still active?
still_active: true
license: GNU GPL
taxonomy:
  creators: [Anna Gray, Dave Brown]
  plateforms: [GNU Linux, Windows, Mac, web]
  categories: [Video, Sound, 3D]
  tags: [generator, light, monotask]
project_url: http://example.com/
extra_links:
  youtube: http://example.com/
  forum: http://example.com/
  demos: http://example.com/
  tutorials: http://example.com/
technology: Python
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Markdown is allowed here. The short description describes the project briefly, it should not be longer than one or two sentences.

## Detailed Description

This is the long description, it can be as long as you need. Describing the project and your experience in details. It should also briefly describes how the attached pictures / video / sounds has been made.
