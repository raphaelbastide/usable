---
project_name: Alchemy
version: Beta 008
# Is the project developpement still active?
still_active: true
license: GNU GPL
taxonomy:
  creators: Karl D.D. Willis et Jacob Hina
  plateforms: MacOsX, Windows et Linux
  categories: images 
  tags: drawing
project_url: http://al.chemy.org/
extra_links:
  forum: http://al.chemy.org/forum/
technology: Java
# Operating system used for the test
tested_on: OS X Yosemite 10.10.5
---

## Short Description

Alchemy est un logiciel libre de création des dessins expérimentales, ses outils ne sont pas conventionnels et utilisent nos manières et façons d'utiliser notre ordinateur personnel. On ne peut ni revenir en arrière, ni sauvegarder un travail en cours.

## Detailed Description

Alchemy est un logiciel libre de dessin (voir de dessin abstrait). Ce logiciel possède quelques particularités : Il est impossible de revenir en arrière, il est impossible d’enregistrer un travail en cours et aucun de ces outils n’est un outils conventionnels (comme pourrait en avoir Gimp ou encore Photoshop). Vous pouvez tout de même exporter votre projet sous SVG, PDF, PNG et JPG. Les outils proposaient par Alchemy sont différents et chacun d’eux possèdent leur particularité d’utilisation, allant de la vitesse de déplacement de votre souris à l’utilisation de votre micro (il est nécessaire d’essayer chaque outils avant de pouvoir maitriser complètement leur utilisation). Ils possèdent leur propre barre de réglages, celle-ci génère plus ou moins de changements sur son utilisation. En plus de ses outils Alchemy possède des effets. Ces effets peuvent être appliqués simultanément ou non, et chaque effet peut modifier un outils, autant dire qu’il y a pléthore de possibilités quand vous vous amusez avec les nombreux réglages en même temps (ou non). De plus Alchemy possède un système simple de calque ; vous pourrez choisir de dessiner en dessous ou au dessus de ce que vous avez déjà réalisé. Vous pourrez aussi choisir l’épaisseur de votre trait, sa couleur et son opacité. Quelques modifications sont possible très simplement. Il est par exemple possible de créer un ensemble de « shapes » afin de les utiliser dans le logiciel. Étant écrit en Java, il est aussi possible d’avoir une action sur les outils eux-mêmes. Je conseillerais aussi d'utiliser le logiciel avec une tablette graphique. Certains outils utilisant la pression et la vitesse de déplacement. L'interface de ce logiciel semble avoir été réalisé pour ce genre d'utilisation. Abandonnez vos pads et souris.