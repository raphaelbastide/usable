---
project_name: Substrack Studio
version:
# Is the project developpement still active?
still_active: true
license:  MIT
taxonomy:
  creators: [James Halliday]
  plateforms: [Web]
  categories: [Sound]
  tags:
project_url: http://studio.substack.net/
extra_links:
  demo: http://studio.substack.net/-/recent
  tutorials: http://studio.substack.net/-/help
technology: JavaScript & HTML
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Substrack Studio is a onligne musical programme with algorithms .
## Detailed Description
In code-music-studio, you make music with a language called javascript. This programme is quiet simple to use is your are familiar with the javascript language and if you're not, there is alot of exemples you can use. You have a graphic interface that aims you to see the wave you make with the algorithms. The only things that miss in this programme, is this possibility to download the sound you make, but there is a history page, where you can see and listen to everything that as being made with substack studio. 
