---
project_name: Inkscape
version: 0.91
# Is the project developpement still active?
still_active: true
license: GNU GPL
taxonomy:
  creators: [See comunity page on launchpad]
  plateforms: [GNU/linux, Windows, Mac]
  categories: [Image]
  tags: [vector, cartography, typography]
project_url: http://inkscape.org/
extra_links:
  addons: https://inkscape.org/en/download/addons/
  tutorials: http://inkscapetutorials.org/
technology: C++
# Operating system used for the test
tested_on: GNU/Linux
---

## Short Description

Inkscape is a wonderful vector drawing software, perfect for illustration, cartography, typographic drawing, logos, posters…

## Detailed Description

Inkscape is an open-source vector graphics editor similar to Adobe Illustrator, Corel Draw, Freehand, or Xara X. What sets Inkscape apart is its use of Scalable Vector Graphics (SVG), an open XML-based W3C standard, as the native format.

Inkscape offers a set of precise tools such as its pen tool and an advanced caligraphy tool. It also comes with a lot of interesting features to experiment and hack: addons (extentions), scripts, filter editor, and all kind of generators.

The development of Inkscape is also active and the comunity is huge: (tutorials, wikis, video demo). That makes this software a really complete tool for all kind of creators.
