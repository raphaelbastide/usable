---
project_name: 64 Stitches
version: 1.0
current_version_release_date: 2015-08-04
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-12-03
project_first-release: 2015-08-04
# Is the project developpement still active?
still_active: true
license: MIT
taxonomy:
  creators: [Mariko Kosaka]
  plateforms: [web]
  categories: [generator]
  tags: [generator, web]
project_url: http://64sts.com/
extra_links:
   github: https://github.com/kosamari/64sts
technology: HTML, JavaScript
installation_process: >

# Boolean value: 'true' or 'false'
installation_needs_command_line: false
---

## Short Description
64 Stitches is a pattern genrerator language.


## Detailed Description
With 64 stitches you can generate geometrical pattern. You can use the pattern with Knitic, witch is an other software create by Varvara Guljajeva & Mar Canet, that allow you to knitting  with machine.
