# Usable

Libre / open source tools for creation, forked from [esadhar Usable wiki](https://gitlab.com/esadhar/usable/-/wikis/liste-outils)

## Video
- **Openshot** https://www.openshot.org/
- **traGtor** (ui for ffmpeg) 
- **Kdenlive** (logiciel de montage vidéo)
- **Natron** (compositing)
- **ezthumb** http://sourceforge.net/projects/ezthumb/
- **Avidemux** http://fixounet.free.fr/avidemux/download.html
- **Cinelerra** http://heroinewarrior.com/cinelerra.php 

## Animation
- **Wick** Flash like online editor http://wickeditor.com/
- **Synfig** (animation) http://www.synfig.org/
- **Appartus** (interactive drawings) http://aprt.us/

## VJ / Live image / Livecoding
- **Orca** https://100r.co/site/orca.html
- **IBNIZ** https://github.com/viznut/IBNIZ/commits/master (licence à clarifier)
- **SuperCollider** - **Substack’s studio** http://studio.substack.net/
- More live coding resources http://toplap.org/
- http://wavepot.com
- https://sonic-pi.net/
- https://sciss.github.io/Mellite/

## 3D
- **OpenSCAD** (make 3D models with code)
- **Sculptgl** (webgl sculpting webapp)
- **Blender**
- **Antinomy** http://www.mattkeeter.com/projects/antimony/3/
- **OpenJsScad** http://openjscad.org/
- **FracPlanet** http://sourceforge.net/projects/fracplanet/
- **Mandelbulber** http://mandelbulber.com/
- **tesseract** moteur de jeu 3D FPS http://tesseract.gg/
- **eureka doom editor** http://eureka-editor.sourceforge.net/
- **FreeCAD** http://www.freecadweb.org/
- **Antimony** http://www.mattkeeter.com/projects/antimony/3/
- **Openmeca** http://d.a.d.a.pagesperso-orange.fr/index.html
- **OpenTeddy** http://openteddy.sourceforge.net/
- **Make Human** http://www.makehuman.org/
- **create3000** http://create3000.de/ (animation, rendering, VR)
- http://gmsh.info

## 3D Painting 
- **CrackArt** (inintéressant pour le moment ) 

## Type design
- **Qglif** https://github.com/mfeq/Qglif
- **F%NT** https://f-nt.eu/
- **metapolator** (générateur de font)
- **Birdfont**
- http://trufont.github.io/
- **Glyphr studio**
- http://www.metaflop.com/modulator (modulateur typo en ligne)
- http://doc.robofont.com (pas sûr qu'il soit libre)
- Metafont (ex. https://github.com/Antoine-Gelgon/Ocr-PBI)
- fontforge
- bitmap generator http://www.angelcode.com/products/bmfont/
- **webify** A command line tool to convert ttf file to woff, eot & svg files https://github.com/ananthakumaran/webify

## Publishing
- [Scribus](http://scribus.net/)
- TeX
- [TeX.js](https://davidar.io/TeX.js/)
- [HTML2print](http://osp.kitchen/tools/html2print/)
- [Even](http://xxyxyz.org/even/)
- http://automatic.ink/ (littérature algorithmique, utilisations de différents type de langage informatique)
- Pagebot https://github.com/TypeNetwork/PageBot
- https://evanbrooks.info/bindery/

## Editor/Text
- haroopad (markdown/html)
- Xjournal (text/pdf)
- http://casual-effects.com/markdeep/
- fist http://www.fourmilab.ch/fist/

## Code
- http://snap.berkeley.edu/
- http://atom.io/
- https://vscodium.com/
- Processing
- http://www.praxislive.org/ (turns Processing to live coding)
- **Drawbot** http://www.drawbot.com/

## Audio
- **Waveform playlist** https://github.com/naomiaro/waveform-playlist
- **Ardour** recording/mixing/editing
- **PureData**
- **Audacity**
- **gnaural** http://gnaural.sourceforge.net/
- **sndcut** (lasercut a record from audio file) https://github.com/kallaballa/sndcut
- **lmms**
- **qsynth** (synthétiseur)
- **hydrogen** (drum machine)
- **jack**
- **rosegarden**
- **sndpeek** sound visualizer http://www.gewang.com/software/sndpeek/
- **Spatium** http://spatium.ruipenha.pt/
- **Audiomass** https://audiomass.co/ Online Audacity-like
- **Helio** https://helio.fm/
- **Zrythm** https://www.zrythm.org Free software + paying

## Generators
- **Triangle** http://www.cs.cmu.edu/~quake/triangle.html
- **ngPlant** (generate plants) http://ngplant.sourceforge.net/
- **arbaro** http://arbaro.sourceforge.net/
- **AA3D** http://aa-project.sourceforge.net/aa3d/
- **TexTuring** http://ivan-murit.fr/texturing.htm
- **64 Stitches** http://64sts.com/
- **jp2a** http://csl.name/jp2a/
- **Comic generator** https://github.com/Triskaideka/robotandhuman http://robotandhuman.neocities.org/
- **Origami Simulator** http://git.amandaghassaei.com/OrigamiSimulator/

## Image
- **Gimp** https://www.gimp.org/
- **Darktable** (Developpement, Retouche photo)
- **Digikam** (Developpement, Retouche photo)
- **Laidout** (vecteur, cartoon…)
- **Pencil Code** http://pencilcode.net/
- **Scratch** https://scratch.mit.edu/
- **FireAplaca** ( image editor)
- **Rendera** https://github.com/Mortis69/rendera/
- **AutoTrace** http://sourceforge.net/projects/autotrace/  vectorisation en ligne de commande
- **ImageMagick** http://www.imagemagick.org/script/binary-releases.php
- **SmillaEnlarger** http://sourceforge.net/projects/imageenlarger/
- **G’MIC** (online, comand line and as GIMP plugin) http://gmic.eu/ - [recent news](http://opensource.graphics/christmas-is-already-here-for-image-processing-folks/)
- **Sketch-n-sketch** SVG programmatic / manual editor http://ravichugh.github.io/sketch-n-sketch/releases/index.html

## Drawing / Painting
- **Inkscape** (Great vector editor) [filters](http://img11.deviantart.net/bb05/i/2009/316/8/e/inkscape_0_47_filter_guide_by_c_quel.png)
- **Krita** (digital painting / illustration / Image edition)
- **scri.ch** [http://scri.ch]
- **Alchemy** http://al.chemy.org/
- [make8bitart](https://github.com/jennschiffer/make8bitart)
- **Scrawl** https://github.com/sgentle/scrawl
- Tesselations drawing http://www.shodor.org/interactivate/activities/Tessellate/ (more [here](http://tessellations.org/software-info.shtml) )
- **My paint** http://mypaint.org/downloads/
- https://github.com/jasonwebb/2d-differential-growth-experiments
- *Collaborative drawing* https://drawpile.net/
- *Lizard Ladder* http://www.tedwiggin.com/LizardLadder/ (author said by mail free to use and modify)

## UI / UX
- *Penpot* https://penpot.app/
- *Akira* https://github.com/Alecaddd/Akira
- *Quant UX* https://www.quant-ux.com/#/

## Maps
- **gprojector**  transform an equirectangular map image into one of over 100 global and regional map projections http://www.giss.nasa.gov/tools/gprojector/
- **grass** https://grass.osgeo.org/screenshots/
- **Tilestudio** http://tilestudio.sourceforge.net/

## CMS
- [Pico CMS](http://picocms.org/) 
- [Grav](http://getgrav.org/)
- [Processwire](http://processwire.com/)
- [Keshif](http://keshif.me/)
- [Hotglue](http://hotglue.org/)

## File Sync
- owncloud (replace dropbox in your own server)
- Rakoshare (more like bitsync)
- [https://syncthing.net/](syncthing)

## Vizualisation
- **graphviz**
- **Free Plane** (for building mind maps.)
- **Blockgiag** http://blockdiag.com/en/seqdiag/index.html
- **Ditaa** convert diagrams drawn using ascii art http://ditaa.sourceforge.net/
- **freemind** http://freemind.sourceforge.net/wiki/index.php/Screenshots

## Other
- Knitic http://www.knitic.com/ (nécessite une machine a coudre industriel)
- weatherspect http://www.robobunny.com/projects/weatherspect/html/
- Valentina (pattern draft -  textile) http://valentinaproject.bitbucket.org/
- ascii world https://github.com/vain/asciiworld
- Pliage, popup, squelettes http://www.popupcad.org/
- Pligage: Origami patterns https://github.com/oripa/oripa
- Stellarium http://news.softpedia.com/news/stellarium-0-14-2-open-source-planetarium-software-gets-list-of-dwarf-galaxies-498654.shtml
